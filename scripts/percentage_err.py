#! /usr/bin/python
# 
#  Copyright (c) 2013 Indian Institute of Technology, Madras. 
#  All Rights Reserved.

#  This program is free software. You can redistribute and/or modify
#  it in accordance with the terms of the accompanying license
#  agreement.  See LICENSE in the top-level directory for details.

# Script to find the % errors in the timer output w.r.t. the reference outputs
import sys
if len(sys.argv) < 3:
    print "%s <reference output> <actual output>"%(sys.argv[0])
    sys.exit()

fref = file(sys.argv[1],"r")
fact = file(sys.argv[2],"r")

dref = fref.read()
dact = fact.read()

fref.close()
fact.close()

lref = dref.split("\n")
lact = dact.split("\n")

at_ref = {}
slack_ref = {}
for line in lref:
    words = line.split()
    if len(words) == 10:
        if words[0] == "at":
            at_ref[words[1]] = [float(words[i]) for i in range(2,2+8)]
    elif len(words) == 5:
        if words[0] == "slack":
            slack_ref[words[1]+'\t'+ words[2]] = [float(words[i]) for i in range(3,5)]


at_act = {}
slack_act = {}
for line in lact:
    words = line.split()
    if len(words) == 10:
        if words[0] == "at":
            at_act[words[1]] = [float(words[i]) for i in range(2,2+8)]
    elif len(words) == 5:
        if words[0] == "slack":
            slack_act[words[1]+'\t'+words[2]] = [float(words[i]) for i in range(3,5)]

#print slack_act, slack_ref
err = {}
notfound_in_act = []
max_err_at = 0.0
max_err_slack= 0.0
for n in at_ref:
    if n in at_act:
        err_float = [((at_act[n][i] - at_ref[n][i])*100/at_ref[n][i]) for i in range(8)]
        err[n] = ["%f"%(x) for x in err_float]
        max_err_at = max([abs(max(err_float)),abs(min(err_float)),max_err_at])
    else:
        notfound_in_act.append(n)

for n in slack_ref:
    if n in slack_act:
        err_float = [((slack_act[n][i] - slack_ref[n][i])*100/slack_ref[n][i]) for i in range(2)]
        err[n] = ["%f"%(x) for x in err_float]
        max_err_slack = max([abs(max(err_float)),abs(min(err_float)),max_err_slack])
    else:
        notfound_in_act.append(n)

outFile = sys.argv[2]+".err_report"
frep = file(outFile,"w")
frep.write("\nError Report\n------------\nReference : %s\nActual : %s\n(percentage error of actual from the reference)\nMax Err At = %.1f\nMax Err Slack = %.1f"%(sys.argv[1],sys.argv[2],max_err_at, max_err_slack))
notfound_in_ref = []
for n in at_act:
    if n in err:
        errline = "\t".join(err[n])
        frep.write("\n"+n+" :\t"+errline)
    else:
        notfound_in_ref.append(n)

for n in sorted(slack_act.keys()):
    if n in err:
        errline = "\t".join(err[n])
        frep.write("\n"+n+" \t:\t"+errline)
    else:
        notfound_in_ref.append(n)

frep.write("\n\nFollowing nodes from the reference were NOT found in the actual\n---------------------------------------------------------------\n")
if len(notfound_in_act) == 0:
    frep.write("Nil\n")
else:
    frep.write("%s"%(",".join(notfound_in_act)))

frep.write("\n\nFollowing nodes from the actual were NOT found in the reference\n---------------------------------------------------------------\n")
if len(notfound_in_ref) == 0:
    frep.write("Nil\n")
else:
    frep.write("%s"%(",".join(notfound_in_ref)))

frep.close()
f = open(outFile, 'r')
print f.read()

print "Results of compare printed out to %s"%(outFile)
