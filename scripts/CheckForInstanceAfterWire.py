# 
#  Copyright (c) 2013 Indian Institute of Technology, Madras. 
#  All Rights Reserved.

#  This program is free software. You can redistribute and/or modify
#  it in accordance with the terms of the accompanying license
#  agreement.  See LICENSE in the top-level directory for details.

import sys

def main():
    if len(sys.argv) < 1:
        print "Usage: program <netlistfile>"
        return
    
    print "Processing file",sys.argv[1]
    f = file(sys.argv[1],"r")    
    s = f.read()
    f.close()
    lines = s.split("\n")
    
    foundWire = 0
    for line in lines:
        words = line.split(" ")
        if "instance" in words:
            if foundWire == 1:
                print "** Problem. Instance declared after wire."
                print "-- Line: ",line
        if "wire" in words:
            foundWire = 1
    
    print "Done."

if __name__ == "__main__":
    main()
