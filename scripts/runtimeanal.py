#! /usr/bin/python
# 
#  Copyright (c) 2013 Indian Institute of Technology, Madras. 
#  All Rights Reserved.

#  This program is free software. You can redistribute and/or modify
#  it in accordance with the terms of the accompanying license
#  agreement.  See LICENSE in the top-level directory for details.

# Reads the runtime.log file and loads the data into memory
f = file("runtime.log","r")
s = f.read()
records = s.split("=== New Run ===")
print len(records),"records found."
data = {
    "parseCellLibrary":[],
    "parseNetlist":[],
    "timingAnalysis":[],
    "main":[],
    "message":[],
    "toolOutput":[]
}

for key in data:
    data[key] = len(records)*[0]
    
for r in range(len(records)):
    record = records[r]
    lines = record.split("\n")
    wordtable = [line.split() for line in lines]
    for l in range(len(lines)):
        words = wordtable[l]
        if len(words) == 3:
            data[words[0]][r] = words[2]

print "data is available in the 'data' variable. Modify runtimeanal.py to do more analysis"
