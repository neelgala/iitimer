# 
#  Copyright (c) 2013 Indian Institute of Technology, Madras. 
#  All Rights Reserved.

#  This program is free software. You can redistribute and/or modify
#  it in accordance with the terms of the accompanying license
#  agreement.  See LICENSE in the top-level directory for details.

for file in ../tau2013_contest_benchmarks.v1.0/*
do	
	echo "*********************">>out
	echo $file >> out
	echo "No of wire = ">>out
	grep -c "wire" $file >> out
	grep -n "wire" $file >> out
done
