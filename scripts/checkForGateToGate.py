# 
#  Copyright (c) 2013 Indian Institute of Technology, Madras. 
#  All Rights Reserved.

#  This program is free software. You can redistribute and/or modify
#  it in accordance with the terms of the accompanying license
#  agreement.  See LICENSE in the top-level directory for details.


import sys

def main():
    if len(sys.argv) < 2:
        print "Usage: program <netlist file> [nodes list file]"
        return
    
    print "Processing file",sys.argv[1]
    f = file(sys.argv[1],"r")    
    s = f.read()
    f.close()
    lines = s.split("\n")
    nodes = []
    
    instanceCount = 0
    
    for line in lines:
        words = line.split(" ")
        if "instance" in words:
            instanceCount += 1
            for word in words:
                p = word.find(":")
                if p > -1:
                    nodes.append(word[p+1:])
    
    print instanceCount, "instances processed."
    if (len(sys.argv)> 2):
        print "Writing out all nodes connected to gate terminals to",sys.argv[2]
        fout = file(sys.argv[2],"w")
    else:
        print "Writing out all nodes connected to gate terminals to out.txt"
        fout = file("out.txt","w")
    
    for node in nodes:
        fout.write(node+"\n")
    
    fout.close()
    
    flag = 0
    for node in nodes:
        if nodes.count(node) != 1:
            print "Problem",node
            flag = 1
    if flag == 0:
        print "Peace !! There are no gate to gate direct connections."
    else :
        print "Gate to gate direct connections exist"
    print "Done."

if __name__ == "__main__":
    main()
