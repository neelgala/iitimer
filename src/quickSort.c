// quickSort.c
// http://www.comp.dit.ie/rlawlor/Alg_DS/sorting/quickSort.c
#include <stdio.h>

#define STRING_SIZE 32

void quickSort( char a[][STRING_SIZE], int[], int, int);
int partition( char a[][STRING_SIZE], int[], int, int);


void main() 
{
	char a[][STRING_SIZE] = { \
	"apple",\
	"mango",\
	"orange",\
	"orange",\
	"arial",\
	"israel",\
	"F21",\
	"windows8" \
	};

	int a_indx[] = { \
	0, \
	1, \
	2, \
	3, \
	4, \
	5, \
	6, \
	7 \
	};
	
	int a_num = 8;
	
	int i;
	printf("\n\nUnsorted array is:  ");
	for(i = 0; i < a_num; ++i)
		printf(" %s ", a[a_indx[i]]);

	quickSort( a, a_indx, 0, a_num - 1);

	printf("\n\nSorted array is:  ");
	for(i = 0; i < a_num; ++i)
		printf(" %s ", a[a_indx[i]]);

}



void quickSort( char a[][STRING_SIZE], int a_indx[], int l, int r)
{
   int j;

   if( l < r ) 
   {
   	// divide and conquer
        j = partition( a, a_indx, l, r);
       quickSort( a, a_indx, l, j-1);
       quickSort( a, a_indx, j+1, r);
   }
	
}


#define qsort_get_string(x) a[x]

int partition( char a[][STRING_SIZE], int a_indx[], int l, int r) {

   int i, j, t;
   int pivot = a_indx[l];
   i = l; j = r+1;
		
   while( 1)
   {
   	//do ++i; while( (qsort_greater_than(i,pivot) != 1) && i <= r );
	do ++i; while( (strcmp(qsort_get_string(a_indx[i]),qsort_get_string(pivot)) <= 0) && i <= r );
   	do --j; while( strcmp (qsort_get_string(a_indx[j]), qsort_get_string(pivot)) > 0 );
   	if( i >= j ) break;
   	t = a_indx[i]; a_indx[i] = a_indx[j]; a_indx[j] = t;
   }
   t = a_indx[l]; a_indx[l] = a_indx[j]; a_indx[j] = t;
   return j;
}








